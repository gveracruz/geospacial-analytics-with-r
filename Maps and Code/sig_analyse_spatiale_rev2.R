

# Pour avoir l'aide de packages

library(help = "GGally")
?GGally
?glm


# installer remotes, nous utiliserons remotes pour installer les autres packages
install.packages("remotes")

# les packages du CRAN
pkgs <- c("devtools",
          "dplyr", 
          "fields",
          "GGally", 
          "gstat", 
          "gtsummary", 
          "GWmodel", 
          "knitr", 
          "mapsf", 
          "plotly",
          "raster",
          "rmarkdown", 
          "rmdformats",
          "sf",
          "spData",
          "spdep", 
          "stars",
          "terra", 
          "tidyverse", 
          "tmap",
          "xaringan", 
          "xaringanthemer")
# Install des packages sur le CRAN
# (les packages déjà présents ne seront pas réinstallés)
remotes::install_cran(pkgs)


# Les packages hors CRAN
remotes::install_github("Nowosad/spDataLarge")
remotes::install_github("Nowosad/supercells")
remotes::install_github("geocompr/geocompkg")



###################################################################################################################################





library(tidyverse)
library(sf)
library(tmap)
library(plotly)
library(gtsummary)
library(GGally)
library(GWmodel)
library(spdep)
# install.packages("reshape2")
library(reshape2)

# Import et préparation de la base
# 
# La base est disponible et décrite ici : https://www.data.gouv.fr/fr/datasets/demandes-de-valeurs-foncieres-geolocalisees/
#   
#   ### Import de la base brute
#   
#   On importe directement le csv dans R (pour le département 17) :



      # CHARGER LES DONNEES



data <- read_csv("https://files.data.gouv.fr/geo-dvf/latest/csv/2020/departements/17.csv.gz")
head(data) #Pour vérifier que l'import est correct
View(data)
dim(data)
colnames(data)
put(names(data))
sapply(data,class)

      # PREPARER LES DONNEES


library(tidyverse)
# Filtre sur les ventes de maisons à sur l'ile de Ré avec coordonnées géographiques

dataRe <- data %>% 
  filter(nom_commune %in% c("Ars-en-Ré",
                            "La Flotte",
                            "Saint-Martin-de-Ré",
                            "Loix",
                            "Saint-Clément-des-Baleines",
                            "Le Bois-Plage-en-Ré",
                            "Rivedoux-Plage",
                            "La Couarde-sur-Mer",
                            "Sainte-Marie-de-Ré",
                            "Les Portes-en-Ré") & 
           nature_mutation == "Vente" & 
           type_local == "Maison" &
           !is.na(longitude) & 
           !is.na(surface_terrain) &
           !is.na(valeur_fonciere))

# Conversion en sf (st_as_sf: convert foreign object to an sf object: function of the sf package) 
library(sf)

dataSf <- dataRe %>% 
  st_as_sf(coords = c("longitude","latitude"), 
           crs = 4326)
plot(st_geometry(dataSf)) #st_geometry returns an object of class sfc; assigning geometry to a data.frame creates an sf object, assigning it to an sf object replaces the geometry list-column.



# Import du fond de carte en shapefile
# Site téléchargement des fonds de carte : https://geoservices.ign.fr/telechargement

re <- st_read("EPCI.shp") #Communautés de communes de la charente maritime
re = re %>% filter(ID == "EPCI____0000002150236738") #on ne garde que l'ile de Ré

# Cartographie
library(tmap)
tmap_mode("view")
tm_shape(re) + 
  tm_polygons(col = "lightblue")+
  tm_shape(dataSf) + 
  tm_dots(col = "red")

re = st_cast(re, "MULTILINESTRING") #pour tracer les contours des littoraux
tm_shape(re) + 
  tm_lines(col = "black")+
  tm_shape(dataSf) + 
  tm_dots(col = "red")

# Ajout d’une variable contextuelle : distance au littoral

re <- st_transform(re, crs = 4326)#Forcer le code pour calculer les distances
dataSf$dist_litt <- st_distance(dataSf, re) %>% 
  as.numeric()

# Map intéractive
dataSf = dataSf %>% dplyr::mutate("adresse_nom_voie" = str_to_title(adresse_nom_voie))
tm_shape(re) + 
  tm_lines(col = "black")+
  tm_shape(dataSf) + 
  tm_bubbles(size = 0.05,col = "dist_litt", 
             border.col = "black", border.alpha = .5, 
             style="fixed", breaks=c(-Inf, seq(50, 1000, by=150), Inf),
             palette="-YlOrRd", contrast=1, 
             title.col="Distance au littoral", id="nom_commune", 
             popup.vars=c("Rue"="adresse_nom_voie","Valeur foncière"="valeur_fonciere", 
                          "Distance au littoral"="dist_litt", "Surface bati"="surface_reelle_bati",
                          "Surface terrain"="surface_terrain",
                          "Nombre de pièces" = "nombre_pieces_principales"
             ))
tm_shape(re) + 
  tm_lines(col = "black")+
  tm_shape(dataSf) + 
  tm_bubbles(size = 0.05,col = "dist_litt", 
             border.col = "black", border.alpha = .5, 
             style="quantile",
             palette="-YlOrRd", contrast=1, 
             title.col="Distance au littoral", id="nom_commune", 
             popup.vars=c("Rue"="adresse_nom_voie","Valeur foncière"="valeur_fonciere", 
                          "Distance au littoral"="dist_litt", "Surface bati"="surface_reelle_bati",
                          "Surface terrain"="surface_terrain",
                          "Nombre de pièces" = "nombre_pieces_principales"
             ))

tm_shape(re) + 
  tm_lines(col = "black")+
  tm_shape(dataSf) + 
  tm_bubbles(size = "valeur_fonciere",col = "dist_litt", 
             border.col = "black", border.alpha = .5, 
             style="quantile",
             palette="-YlOrRd", contrast=1, 
             title.col="Distance au littoral", id="nom_commune", 
             popup.vars=c("Rue"="adresse_nom_voie","Valeur foncière"="valeur_fonciere", 
                          "Distance au littoral"="dist_litt", "Surface bati"="surface_reelle_bati",
                          "Surface terrain"="surface_terrain",
                          "Nombre de pièces" = "nombre_pieces_principales"
             ))



# Exploration des variables : verifier les valeurs aperante
# Distribution de la variable dépendante (prix de vente)

library(plotly)

plot_ly(dataSf, x = ~valeur_fonciere) %>% add_histogram()



# Suppression d’une valeur aberrante (garder les valeurs/prix superieur a 1000)

dataSf <- dataSf %>% filter(valeur_fonciere > 1000)

# Distribution très dissymétrique (faire me log des valeurs pour normaliser la distribuition = redresser)

plot_ly(dataSf, x = ~log(valeur_fonciere)) %>% add_histogram()

# C’est mieux !

# Distribution des variables indépendantes (je log les variables explicative pour faire un model log-log où on log les varaiables independente et les variables dependente)

a <- plot_ly(dataSf, x = ~log(dist_litt)) %>% add_histogram()
b <- plot_ly(dataSf, x = ~log(surface_reelle_bati)) %>% add_histogram()
c <- plot_ly(dataSf, x = ~log(surface_terrain)) %>% add_histogram()
subplot(a,b,c)


# Suppression des maisons vraisemblablement trop petites
dataSf <- dataSf %>% filter(surface_reelle_bati > 10)

# Création des variables log (pour faciliter la carto par la suite)
dataSf$log_valeur_fonciere <- log(dataSf$valeur_fonciere)
dataSf$log_dist_litt <- log(dataSf$dist_litt)
dataSf$log_surface_reelle_bati <- log(dataSf$surface_reelle_bati)
dataSf$log_surface_terrain <- log(dataSf$surface_terrain)



      # EXPLORATION DES RELATIONS BI-VARIES



# Relations bivariées - formes fonctionelles

ggplot(dataSf, aes(x=log(dist_litt), y=log(valeur_fonciere))) + 
  geom_point() + geom_smooth()

# relation moyenne : regression linaire classique c'est basé sur la moyenne, plus en s'enloigne tu litoral les prix baisse

ggplot(dataSf, aes(x=log(surface_reelle_bati), y=log(valeur_fonciere))) + 
  geom_point() + geom_smooth()

ggplot(dataSf, aes(x=log(surface_terrain), y=log(valeur_fonciere))) + 
  geom_point() + geom_smooth()




    # REGRESSION LINAIRE CLASSIQUE




# Modèle log-log global (MCO) (model global, cela veut dire base sur la moyenne)
# lm (linear model = variable depedente continue quantitative, si la variable est qualitative/categorielle on utilise un model glm = generalized linear model)
# variable qualitative nominal (pour de variable categorial non ordoné)

mco <- lm(log_valeur_fonciere ~ log_dist_litt + log_surface_reelle_bati + log_surface_terrain, data = dataSf)

install.packages("gtsummary") # package pour sortir des resultats sous la forme d'un beaux tableau
library(gtsummary)



summary(mco)

# si la distance augmente en s'eloignant du litoral, le prix deminiui de 0.8%

install.packages("car") # pour utiliser la function add_vif et construire une table des resultats
library(car)

mco %>%
  tbl_regression(intercept = TRUE) %>% 
  add_vif() 

# Utiliser le package ggl pour representer le tableau ci-dessous autrement
library(GGally)
ggcoef_model(mco)


# Cartographie des résidus (si les model étair parfait, tous les résidu serait égal à 0)
# Si le résidu est négative, le model surestime la valeur du prix

# Comme est en log, on doit faire la exponentiel des résidu pour revenir aux valeurs tel quel pour miex interpreter

dataSf$resMco <- mco$residuals

tm_shape(dataSf) + tm_dots(col = "resMco", style = "quantile")

# Resultats : repartition des résidu non homogene. Le model est inparfait : probleme de multiculinarité ? Probleme de valeur aberante ?




        #   MODELE GWR (GEOGRAPHYCALLY WEIGHTED REGRESSION)


# Première chose à faire : convertir l’objet sf en objet sp
library(sf)

dataSp <- as_Spatial(dataSf) # le package GWmodel n'est pas compatible avec 'sf'

# Construction de la matrice de distances: utuliser le package GWmodel

matDist <- gw.dist(dp.locat = coordinates(dataSp))

# Comparaison de deux pondérations spatiales : exponentielle et bicarrée :
  
  # Exponential
  nNeigh.exp <- bw.gwr(data = dataSp, approach = "AICc",
                       kernel = "exponential",
                       adaptive = TRUE,
                       dMat = matDist,
                       formula = log_valeur_fonciere ~ log_dist_litt + log_surface_reelle_bati + log_surface_terrain)
              # il estime 25 voisins
  
  
  # Bisquare
  nNeigh.bisq <- bw.gwr(data = dataSp, approach = "AICc", 
                        kernel = "bisquare", 
                        adaptive = TRUE, 
                        dMat = matDist, 
                        formula = log_valeur_fonciere ~ log_dist_litt + log_surface_reelle_bati + log_surface_terrain)
            #estime 151 voisins
  
# Estimation de la GWR
  
  # Avec pondération exponential
  GWR.exp <- gwr.basic(data = dataSp, bw = nNeigh.exp, kernel = "exponential", adaptive = TRUE,  dMat = matDist, formula = log_valeur_fonciere ~ log_dist_litt + log_surface_reelle_bati + log_surface_terrain)
  
  # Avec pondération bisquare
  GWR.bisq <- gwr.basic(data = dataSp, bw = nNeigh.bisq, kernel = "bisquare", 
                        adaptive = TRUE,  dMat = matDist, 
                        formula = log_valeur_fonciere ~ log_dist_litt + log_surface_reelle_bati + log_surface_terrain)
  
# Comparaison des deux calibrations :
  
  diagGwr <- cbind(
    rbind(nNeigh.exp,nNeigh.bisq),
    rbind(GWR.exp$GW.diagnostic$gw.R2,GWR.bisq$GW.diagnostic$gw.R2),
    rbind(GWR.exp$GW.diagnostic$AIC,GWR.bisq$GW.diagnostic$AIC)) %>% 
    `colnames<-`(c("Nb Voisins","R2","AIC")) %>% 
    `rownames<-`(c("EXPONENTIAL","BISQUARE"))
  diagGwr
  
# Conclusion: 
  # La GWR avec pondération exponentielle est la plus performante (il maximise le R2 et minimise le AIC)
  
#Interprétation des résultats bruts de la GWR :
    
  GWR.exp
  
  
# Première information : le R2 du modèle GWR est > au R2 du modèle MCO (plus grande capacité à expliquer la variance de Y).
# Seconde information : il semble exister une non-stationnarité spatiale, et même des inversions de signes pour la variable distance au littoral
  
# Il faut maintenant cartographier les betas de chaque variable pour décrire cette non-stationnarité spatiale.
  
  # Fonction de cartographie automatique des coefficients GWR
      # The t statistic is the coefficient divided by its standard error
  
  mapGWR <- function(spdf,var,var_TV,legend.title = "betas GWR",main.title, dot.size = 0.3) {
    tv <- spdf[abs(var_TV)>1.96,]
    tm_shape(spdf) +
      tm_dots(var, title = legend.title, size = dot.size) +
      tm_shape(oleron) + tm_lines() +
      tm_shape(tv) + tm_dots(col="grey40") +
      tm_layout(title = main.title, legend.title.size =0.9, inner.margins = .15) 
  }
  
  # Planche cartographique des 3 variables : aplication de la function cree
  
  tmap_mode("plot")
  a <- mapGWR(GWR.exp$SDF, var = "log_dist_litt",var_TV = GWR.exp$SDF$log_dist_litt_TV,
              main.title = "Distance au littoral")
  b <- mapGWR(GWR.exp$SDF, var = "log_surface_reelle_bati",var_TV = GWR.exp$SDF$log_surface_reelle_bati_TV,
              main.title = "Surface bâtie")
  c <- mapGWR(GWR.exp$SDF, var = "log_surface_terrain",var_TV = GWR.exp$SDF$log_surface_terrain_TV,
              main.title = "Surface terrain")
  
  tmap_arrange(a,b,c)
  
  
  
# Interprétation des cartes de betas GWR
  
# L’interprétation des cartes GWR est la partie la plus délicate, mais aussi la plus intéressante. Nous présentons d’abord une clé de lecture théorique et générique, puis une application à nos données.
# Interprétation théorique : le poids des contextes locaux
  
  #..... VOIR LE PLUS DE DETAIL DANS LE CAHIER
  
  # Interprétation empirique : recherche de spécificités locales inobservées
  
  
# Pour aller plus loin
# La GWR multiscalaire (multiscale GWR)
  # 
  # Il n’y a pas de raison de penser que tous les prédicteurs agissent sur le prix à la même échelle 
  # (c’est-à-dire selon un même schéma de voisinage). Certains processus peuvent être locaux, d’autres globaux. 
  # Récemment, une extension de la GWR a été proposée, permettant de relâcher cette hypothèse d’égalité des échelles : 
  #   la GWR multiscalaire (MGWR, Fotheringham et al., 2017). Le principe est simple : un algorithme optimise le choix de 
  # la bandwidth pour chaque prédicteur, en fonction des autres. Il en résulte un modèle souvent mixte.
  
  source("gwr.multiscale_T.r") 
  
  # On lance la MGWR
  MGWR <- gwr.multiscale(formula = log_valeur_fonciere ~ log_dist_litt + 
                           log_surface_reelle_bati + log_surface_terrain,
                         data = dataSp, kernel = "exponential",
                         predictor.centered=rep(T, 3), # centrage des prédicteurs
                         adaptive = TRUE,
                         bws0 = rep(1,4)) # BW minimum pour l'optimisation
  
  
  mgwr.bw  <- round(MGWR[[2]]$bws,1) # Nombre de voisins pour chaque prédicteur
  mgwr.bw
  
  
  # Exploration des résultats statistiques
  print(MGWR)
  
  #
  # On constate que si l’effet de la surface bâtie sur les prix agit de façon assez globale à l’échelle de l’île, les deux autres prédicteurs agissent de manière beaucoup plus locales. Ainsi par exemple, l’effet de la distance au littoral relève d’un processus très localisé.
  # Cartographie des résultats
  
  a <- mapGWR(MGWR$SDF, var = "log_dist_litt",var_TV = MGWR$SDF$log_dist_litt_TV,
              main.title = "Distance au littoral (bw = 77)")
  b <- mapGWR(MGWR$SDF, var = "log_surface_reelle_bati",var_TV = MGWR$SDF$log_surface_reelle_bati_TV,
              main.title = "Surface bâtie (bw = 368)")
  c <- mapGWR(MGWR$SDF, var = "log_surface_terrain",var_TV = MGWR$SDF$log_surface_terrain_TV,
              main.title = "Surface terrain (bw = 23)")
  
  tmap_arrange(a,b,c)
  
  
  
  
  
  #---------
  
  sessionInfo()